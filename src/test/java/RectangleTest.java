import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

public class RectangleTest {
    private Shape shape;

    @Test
    public void shouldCalcIntegers() {
        // given
        shape = new Rectangle(5, 4);

        // when
        float area = shape.calculateArea();

        // then
        Assert.assertEquals(20, area, 0.01f);
    }
    @Test

    public void shouldCalcFloat() {
        // given
        shape = new Rectangle(5.4f, 4.3f);

        // when
        float area = shape.calculateArea();

        // then
        Assert.assertEquals(23.22, area, 0.01f);
    }




}
