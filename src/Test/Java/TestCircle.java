import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.*;

public class TestCircle {


        private Shape shape;
        @Test
        public void shouldCalculateArea(){
        shape = new Circle(2);
        float area = shape.calculateArea();
        Assert.assertEquals(12.56f,area, 0.1f);
    }
}
