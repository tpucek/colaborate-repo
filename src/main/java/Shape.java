public interface Shape {
    float calculateArea();
}
