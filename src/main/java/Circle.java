public class Circle implements Shape {
  public int radius;

    public void setRadius(int radius) {
        this.radius = radius;
    }

    public int getRadius() {
        return radius;
    }

    @Override
    public String toString() {
        return "Circle{}";
    }

    public Circle(int radius) {
        this.radius = radius;
    }

    public float calculateArea() {
      Float f =  (float)(Math.PI)*radius*radius;
      return f;
    }


}
